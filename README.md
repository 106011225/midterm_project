# Software Studio 2019 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : [software studio mid project]
* Key functions (add/delete)
    1. [Shopping website]
    2. [Product page]
    3. [shopping pipeline]
    4. [user dashboard]

* Other functions (add/delete)
    1. [Sign Up/In with Google]
    2. [Chrome notification]
    3. [CSS animation]

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

## Website Detail Description

# 作品網址：[https://software-studio-mid-proj-1f825.firebaseapp.com]

# Components Description : 
1. [Shopping website] : [分三種類行的產品]
2. [Product page] : [可以點擊個別商品看大圖，並放入購物車]
3. [shopping pipeline] : [放入購物車後，點擊右上方email進入個人頁面，cart中可送出/取消訂單，賣方會收到訂單]
4. [user dashboard] : [有購物車，販賣的商品，訂單，新增/刪除商品]

# Other Functions Description(1~10%) : 
1. [Sign Up/In with Google] : [google帳號登入]
2. [Chrome notification] : [接到訂單時會有提醒]
3. [CSS animation] : [首頁左上標題，hover時有動畫]
...

## Security Report (Optional)
透過database.rules限制一般人的寫入權限
