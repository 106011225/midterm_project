function initIdAddItem(){

    var btnSubmit = document.getElementById('btn_submit');
    var txtProductName = document.getElementById('input_productname');
    var txtPrice = document.getElementById('input_price');
    var txtQuantity = document.getElementById('input_quantity');
    var radioCategoryT = document.getElementsByName('optradio');
    var txtDescription = document.getElementById('input_description');
    
    
    //handle sign out 
    document.getElementById('btn_signout').addEventListener("click", function (){
        firebase.auth().signOut().then(function() {
            alert("Sign-out successuful");
        }).catch(function(error) {
            alert(error.messenge);
        });
    });
        
   /* 
            //notification
            var permission;
            if (Notification.permission === 'default' || Notification.permission === 'undefined') {
                Notification.requestPermission(function (p) {permission = p;});
            }
            firebase.database().ref("users/" + firebase.auth().currentUser.uid + "/orderList").on('value',function(){
                 var notifyConfig = {
                    body: ""
                };
                if (permission === 'granted') { 
                     var notification = new Notification('Hi there! You just got a new order!', notifyConfig); 
                }
            });    
    */
    var img = new Image();
        img.src = "http://placehold.it/900x400";
        document.getElementById('btn_upload').onchange = function(e){
            img.src = URL.createObjectURL(this.files[0]);
            document.getElementById('itemimg').src = img.src;
        };



    btnSubmit.addEventListener('click', function(){
    
        // get category #, 3=check neither
        var i;
        for(i=0; i<=3; i++){
            if (radioCategoryT[i].checked == true) break;
        }
       
    



        //valid filled form
        if (txtProductName.value != "" &&
            txtPrice.value != "" &&
            txtQuantity.value != "" &&
            txtDescription.value != "" &&
            i != 3){
            
            var user = firebase.auth().currentUser;
            
            //set itemCategory
            var itemCategory = radioCategoryT[i].value;
           
            //write database goods
            /*
            firebase.database().ref('goods/' + itemCategory).push({
                ProductName:txtProductName.value,
                Price:txtPrice.value,
                Quantity:txtQuantity.value,
                Description:txtDescription.value,
                Owner:user.uid 
            });
            */ 
            
            var newPostKey = firebase.database().ref().child('goods/' + itemCategory).push().key;
            var updates = {};
            updates['goods/' + itemCategory + "/"+ newPostKey] = {
                ProductName:txtProductName.value,
                Price:txtPrice.value,
                Quantity:txtQuantity.value,
                Description:txtDescription.value,
                Owner:user.uid,  
                Category: itemCategory,
                ImgUrl: img.src,
                Comments:null
            };
            firebase.database().ref().update(updates);
            
            
            //write database users
            firebase.database().ref('users/' + user.uid + '/ownedGoods').push({
                path: "goods/" + itemCategory + "/" + newPostKey 
            });
 
            alert("Sumit successful");

            //reset
            txtProductName.value = "";
            txtPrice.value = "";
            txtQuantity.value = "";
            txtDescription.value = "";
        }
        else {
            alert("Please fill out the form correctly.")
        }
    });


}

window.onload = function(){
    initIdAddItem();
};

