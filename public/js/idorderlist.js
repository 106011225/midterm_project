
function initIdShoplistPage(){

    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
         
            //handle sign out 
            document.getElementById('btn_signout').addEventListener("click", function (){
                firebase.auth().signOut().then(function() {
                    alert("Sign-out successuful");
                }).catch(function(error) {
                    alert(error.messenge);
                });
            });
            
            //notification
            var permission;
            if (Notification.permission === 'default' || Notification.permission === 'undefined') {
                Notification.requestPermission(function (p) {permission = p;});
            }
            firebase.database().ref("users/" + firebase.auth().currentUser.uid + "/orderList").on('value',function(){
                 var notifyConfig = {
                    body: ""
                };
                if (permission === 'granted') { 
                     var notification = new Notification('Hi there! You just got a new order!', notifyConfig); 
                }
            });    

            // dynamic list
            var orderListDatabaseRef = firebase.database().ref('users/' + user.uid + '/orderList'); 
            orderListDatabaseRef.on('value',function(snapshot1){

                document.getElementById('tablecontent').innerHTML = "";           

                snapshot1.forEach(function (child){
                    firebase.database().ref(child.val().path).on('value',function(snapshot){
                    
                        document.getElementById('tablecontent').innerHTML += "<tr><td>" + 
                                            snapshot.val().ProductName +
                                            "</td><td>" + snapshot.val().Price +
                                            "</td><td>" + snapshot.val().Description +
                                            "</td></tr>";
                    });
                    
                });
            });
                    


        
        
        
        } 
        else {
            window.location = "index.html"    
        }
    });

}



window.onload = function(){
    initIdShoplistPage();
};

/*
                  <tr>
                    <td>Tiger Nixon</td>
                    <td>System Architect</td>
                    <td>Edinburgh</td>
                    <td>61</td>
                    <td>2011/04/25</td>
                    <td>$320,800</td>
                  </tr>
*/

