
// Delete
function Deletefromlist(userid,itemKey){
   firebase.database().ref("users/" + userid + "/cart/" + itemKey).remove();            
}

function initIdShoplistPage(){

    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
         
            //handle sign out 
            document.getElementById('btn_signout').addEventListener("click", function (){
                firebase.auth().signOut().then(function() {
                    alert("Sign-out successuful");
                }).catch(function(error) {
                    alert(error.messenge);
                });
            });

            // dynamic list
            var cartDatabaseRef = firebase.database().ref('users/' + user.uid + '/cart'); 
            cartDatabaseRef.on('value',function(snapshot1){

                document.getElementById('tablecontent').innerHTML = "";           

                snapshot1.forEach(function (child){
                    firebase.database().ref(child.val().path).on('value',function(snapshot){
                    
                        document.getElementById('tablecontent').innerHTML += "<tr><td>" + 
                                            snapshot.val().ProductName +
                                            "</td><td>" + snapshot.val().Price +
                                            "</td><td>" + snapshot.val().Description +
                                            "</td><td><button type='button' class='btn btn-danger' id='" +
                                            user.uid + "' onclick='Deletefromlist(this.id,this.name)' " +
                                            "name='"+ child.key +"'>Delete</button></td></tr>";
                    });
                    
                });
            });
             



            //chech out 
            var btnCheckout = document.getElementById('btn_checkout');
            btnCheckout.addEventListener('click',function(){
                
                cartDatabaseRef.on('value',function(snapshot1){
                    snapshot1.forEach(function(child){
                        firebase.database().ref(child.val().path).on('value',function(snapshot){
                            var itemOwner = snapshot.val().Owner;

                            firebase.database().ref('users/'+itemOwner+'/orderList').push({
                                path: child.val().path     
                            });
                        
                        });
                    });
                });
                alert("seccessful!");
            });

        
        
        
        } 
        else {
            window.location = "index.html"    
        }
    });

}



window.onload = function(){
    initIdShoplistPage();
};

/*
                  <tr>
                    <td>Tiger Nixon</td>
                    <td>System Architect</td>
                    <td>Edinburgh</td>
                    <td>61</td>
                    <td>2011/04/25</td>
                    <td>$320,800</td>
                  </tr>
*/

