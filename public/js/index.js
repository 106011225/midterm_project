


function initIndexPage(){
    

    // handle login logout appearence 
    firebase.auth().onAuthStateChanged(function (user) {
        
        var menu = document.getElementById('topbar');

        if (user) {
        menu.innerHTML = "<li class='nav-item'><a class='nav-link' href='idprofile.html'>" + user.email 
          + "</a></li><li class='nav-item'><a class='nav-link' id='btn_signout' href=''>Sign Out</a></li>";

            //handle sign out 
            document.getElementById('btn_signout').addEventListener("click", function (){
                firebase.auth().signOut().then(function() {
                    alert("Sign-out successuful");
                }).catch(function(error) {
                    alert(error.messenge);
                });

            }); 
            
            
            
            
            //notification
            var permission;
            if (Notification.permission === 'default' || Notification.permission === 'undefined') {
                Notification.requestPermission(function (p) {permission = p;});
            }
            firebase.database().ref("users/" + firebase.auth().currentUser.uid + "/orderList").on('value',function(){
                 var notifyConfig = {
                    body: ""
                };
                if (permission === 'granted') { 
                     var notification = new Notification('Hi there! You just got a new order!', notifyConfig); 
                }
            });    


        } else {
            menu.innerHTML = "<li class='nav-item'><a class='nav-link' href='signin.html'>Sign In</a></li>";
        }
    });





    var btnCtgy0 = document.getElementById('ctgy0');
    var btnCtgy1 = document.getElementById('ctgy1');
    var btnCtgy2 = document.getElementById('ctgy2');

    SetHtml("Computers_and_Electronics");


    /*
    goodsDatabaseRef.on('value',function(snapshot){
        totalCards = [];

        //alert(snapshot.child[0].val().ProductName);

        snapshot.forEach(function (child){
            
            //alert(txtCategory);
            //var addr = firebase.database().ref().child("Computers_and_Electronics/" + child.key);
            //var addrStr = addr.toString();
            var subAddrStr = "goods/" + txtCategory + "/" + child.key;
            //alert(firebase.database().ref().child(subAddrStr));

           // alert(firebase.database().ref(subAddrStr)); 
            

            totalCards.push( ItemCardHtmlGenerator( subAddrStr ,child ) );
        });            
        document.getElementById('itemCardDiv').innerHTML = totalCards.join('');           
    });
*/
        






}

function SetHtml(txtCategory="Computers_and_Electronics") {
    
    goodsDatabaseRef = firebase.database().ref('goods/' + txtCategory); 
    goodsDatabaseRef.once('value')
        .then(function(snapshot){

            var totalCards = [];
            snapshot.forEach(function (child){
                var subAddrStr = "goods/" + txtCategory + "/" + child.key;
                totalCards.push( ItemCardHtmlGenerator(subAddrStr, child) );
            });
            document.getElementById('itemCardDiv').innerHTML = totalCards.join('');


            goodsDatabaseRef.on('value',function(snapshot1){
                totalCards = [];
                snapshot1.forEach(function (child){
                    var subAddrStr = "goods/" + txtCategory + "/" + child.key;
                    totalCards.push( ItemCardHtmlGenerator( subAddrStr ,child ) );
                });            
                document.getElementById('itemCardDiv').innerHTML = totalCards.join('');           
            });


        })
        .catch(error => console.log(error.messenge));
}

function ItemCardHtmlGenerator(addr,child){

    var object = child.val();
    //var addr = "firebase.database().ref('goods/Computers_and_Electronics/" + child.key + "')" ;

    return "<div class='col-lg-4 col-md-6 mb-4'><div class='card h-100'>"+
        "<img class='card-img-top' src='" + object.ImgUrl + "' alt=''>"+
        "<div class='card-body'><h4 class='card-title'><a href='viewitem.html' " + 
        "onclick='saveItemPath(this.id)'" + "id='"+ addr +"'" + ">" + 
         object.ProductName + "</a></h4><h5>$" + object.Price + "</h5><p class='card-text'>"+ 
        object.Description + "</p></div><div class='card-footer'>"+
        "<small class='text-muted'>&#9733; &#9733; &#9733; &#9733; &#9734;</small></div></div></div>";
}

function saveItemPath(addrStr){
    firebase.database().ref('CurrentViewItemAddr').set({ itemPath: addrStr });
}

window.onload = function(){
    initIndexPage();
};

