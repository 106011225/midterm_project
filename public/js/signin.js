function initSignInPage(){

    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnSignin = document.getElementById('btn_signin');
    var btnGoogle = document.getElementById('btn_google');
    var btnFacebook = document.getElementById('btn_facebook');

    btnSignin.addEventListener('click', function(){
    
        firebase.auth().signInWithEmailAndPassword(txtEmail.value, txtPassword.value)
        .then(function(){
	        //window.location = "index.html";
            history.back();
	    })
        .catch(function(error) {
            alert(error.message);
            txtEmail.value = "";
            txtPassword.value = "";
        });
    
    });


    btnGoogle.addEventListener('click', function(){
    
        var provider = new firebase.auth.GoogleAuthProvider();
        //provider.addScope('profile');
        //provider.addScope('email');
        firebase.auth().signInWithPopup(provider)
            .then(function(result) {
                // This gives you a Google Access Token. You can use it to access the Google API.
                //var token = result.credential.accessToken;
                // The signed-in user info.
                //var user = result.user;
                
                //window.location = "index.html";
                history.back();
            })
            .catch(function(error) {
                alert(error.message);
                txtEmail.value = "";
                txtPassword.value = "";
        });
        
        



    });

    //facebook ...

}

window.onload = function(){
    initSignInPage();
};
