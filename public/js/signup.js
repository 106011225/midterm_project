function initSignUpPage(){

    var txtUsername = document.getElementById('lastname');
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnSignup = document.getElementById('btn_signup');

    btnSignup.addEventListener('click',function(){
        
        firebase.auth().createUserWithEmailAndPassword(txtEmail.value,txtPassword.value)
        .then(function(){
            alert("Successful");
            txtEmail.value = "";
            txtPassword.value = "";
	        window.location = "index.html";
	    })
        .catch(function(error) {
            alert(error.message);
            txtEmail.value = "";
            txtPassword.value = "";
        });
    });

}

window.onload = function(){
    initSignUpPage();
};


