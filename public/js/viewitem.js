
var addr;
var currentUser;

function initViewItemPage (){

    // handle login logout appearence 
    firebase.auth().onAuthStateChanged(function (user) {
        
        var menu = document.getElementById('topbar');

        if (user) {
        menu.innerHTML = "<li class='nav-item'><a class='nav-link' href='idprofile.html'>" + user.email 
          + "</a></li><li class='nav-item'><a class='nav-link' id='btn_signout' href=''>Sign Out</a></li>";

            //handle sign out 
            document.getElementById('btn_signout').addEventListener("click", function (){
                firebase.auth().signOut().then(function() {
                    alert("Sign-out successuful");
                }).catch(function(error) {
                    alert(error.messenge);
                });
            });
            
            //notification
            var permission;
            if (Notification.permission === 'default' || Notification.permission === 'undefined') {
                Notification.requestPermission(function (p) {permission = p;});
            }
            firebase.database().ref("users/" + firebase.auth().currentUser.uid + "/orderList").on('value',function(){
                 var notifyConfig = {
                    body: ""
                };
                if (permission === 'granted') { 
                     var notification = new Notification('Hi there! You just got a new order!', notifyConfig); 
                }
            });    




        } else {
            menu.innerHTML = "<li class='nav-item'><a class='nav-link' href='signin.html'>Sign In</a></li>";
        }
    });


    // set webpage according to clicked item 
    firebase.database().ref('CurrentViewItemAddr').once('value')
    .then(function(snapshot){
        addr = snapshot.val().itemPath;

        firebase.database().ref(addr).once('value')
        .then(function(snapshot){

            document.getElementById('itemcard').innerHTML = setInnerHtml(snapshot.val());


            var currentUser = firebase.auth().currentUser;

            //put to cart event 
            var btnPutToCart = document.getElementById('btn_puttocart');
            btnPutToCart.addEventListener('click',function(){
                if (currentUser){   
                    //write database users
                    firebase.database().ref('users/' + currentUser.uid + '/cart').push({
                         path: addr 
                    });
                    alert("added successful!");
                } 
                else {
                    alert("Please sign in first.");
                    window.location = "signin.html";
                }
            });





        });
    });

/*
    var txtCmt = document.getElementById('input_description');
    var btnCmt = document.getElementById('btn_cmt');
    btnCmt.addEventListener('click',function(){
    
            var currentUser = firebase.auth().currentUser;
                if (currentUser){   
                    if (txtCmt.innerHTML != ""){

                        //write database 
                        firebase.database().ref(addr + "/Comments").push({
                            Cmt:txtCmt.innerHTML
                        });
                    }
                    else {
                        alert("blank messenge");
                    }
                } 
                else {
                    alert("Please sign in first.");
                    window.location = "signin.html";
                }
        
    });
*/

}






function setInnerHtml(item){

    return "<img class='card-img-top img-fluid' src='"+ item.ImgUrl +"' alt=''>" + 
           "<div class='card-body'><h3 class='card-title'>" + item.ProductName +
           "</h3><h4>$" + item.Price + "</h4><p class='card-text'>" + item.Description + 
           "</p><span class='text-warning'>&#9733; &#9733; &#9733; &#9733; &#9734;</span>4.0 stars</div>"

}

window.onload = function (){
    initViewItemPage();
}
 

